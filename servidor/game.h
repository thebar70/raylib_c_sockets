#ifndef GAME_H
#define GAME_H 

#include "raylib.h"

typedef struct {
	int width; /**< Ancho de la pantalla en pixels. */
	int height; /**< Alto de la pantalla en pixels. */
}GameScreen;

/** 
* @brief Mapa del juego
*/
typedef struct {
	Texture2D texture;	/**< Imagen de fondo. */
	int width; /**< Ancho del mapa. */
	int height; /**< Alto del mapa. */
	Rectangle base;	/**< Rectángulo base del mapa. */
}GameMap;


/** 
* @brief Estructura de información para el juego.
*/
typedef struct {
	
	GameMap map; /**< Mapa del juego. */
	GameScreen screen; /**< Pantalla del juego. */
	Texture2D monedaTexture; /*Imagen del objetivo*/
	double minZoom; /**< Nivel minimo de acercamiento. */
	double maxZoom; /**< Nivel maximo de acercamiento. */
	Font font; /**< Fuente para dibujar los textos. */
	Rectangle base_moneda;
	
}Game;

typedef struct 
{
	Vector2 pos;
	Vector2 objetivo;
	int flag;
}InfoGame;

typedef struct 
{
	Game *game;
	InfoGame *info;
}Config;




void Play(char * port);
void configuracionGeneral(Game * game, InfoGame *info);
void InitGame(Game * game,InfoGame *info); 

void EsperarOponente(Config *config,Game *game,InfoGame *info,InfoGame *oponente, char * port);  

void verificarObjetivo(InfoGame *info);
 
void ProcessEvents(Game *game,InfoGame *info, InfoGame *oponente);

void DrawGame(Game * game, InfoGame *info, InfoGame *oponente);

void generarObjetivo(Game *game,InfoGame *info);

void * jumpers(void * argv);

void DrawStatusText(Game * game, const char * text, int size, Color color);

void FinalizeGame();


#endif
