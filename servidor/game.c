#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* Según POSIX 1003.1-2001 */
#include <sys/select.h>
#include <netinet/in.h>
#include <math.h>


#include <sys/socket.h>
#include <pthread.h>

/* Según estándares anteriores */
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "game.h"
#include "raylib.h"


int c;//Descriptor del oponente
pthread_mutex_t mutex; //Mutex que controla la generacion de posiciones de la moneda
fd_set read_fds;
struct timeval t;
Rectangle rectangulo;
int score;
char * mensaje;

void Play(char * port){
    Game *game;//Estructura que contiene informacion de pintado
    InfoGame *info;//Estructura que contiene informacion de la poscion actual
    InfoGame *oponente;//Estrutura de informacion del oponente
    Config *config;
    game=(Game *)malloc(sizeof(Game));
    info=(InfoGame*)malloc(sizeof(InfoGame));
    oponente=(InfoGame*)malloc(sizeof(InfoGame));
    config=(Config*)malloc(sizeof(Config));
    configuracionGeneral(game,info);//Configuracion de inicio
    EsperarOponente(config,game,info,oponente,port);//SE espera hasta que exista un oponente
    InitGame(game,info); //Se configura pintado 
    /**
     * Bucle principal del juego
    */
	while (!WindowShouldClose()) 
	{
	    ProcessEvents(game,info,oponente); 
        DrawGame(game,info,oponente);
        
	}
	FinalizeGame();
}
void configuracionGeneral(Game * game, InfoGame *info){
   
    //Definicion del tamaño de la ventana
    game->screen.width=800;
    game->screen.height=450;
    
    t.tv_sec=0;
    t.tv_usec=10;

    score=0;
    mensaje=(char*)malloc (sizeof(char));
    strcpy(mensaje,"Score: 0");

    pthread_mutex_init(&mutex,NULL);
    //Posicion de la esfera
    info->pos=(Vector2) { (float)game->screen.width/2, (float)game->screen.height/2 };
    info->flag=0;
    //*************************************///
}
void InitGame(Game * game,InfoGame *info) {
    //Cargando la imagen principal en la ventana
    Image mapImage = LoadImage("resources/mapa3.jpg");
    Image monedaImage = LoadImage("resources/moneda.png"); 
    game->map.width = mapImage.width;
	game->map.height = mapImage.height;
    InitWindow(game->screen.width, game->screen.height, "Servidor  "); 
    game->map.base = (Rectangle){0, 0,  mapImage.width, mapImage.height};
    game->map.texture = LoadTextureFromImage(mapImage);
    game->monedaTexture=LoadTextureFromImage(monedaImage);
    game->base_moneda = (Rectangle){0, 0,  monedaImage.width, monedaImage.height};
    game->font = LoadFont("resources/Hemondalisa.ttf");
    SetTextureFilter(game->font.texture, FILTER_BILINEAR);//Establecer el filtro para el trazado de la fuente
    rectangulo=game->base_moneda;//Creamos el regtangulo que contiene la base de la moneda
    rectangulo.x=info->objetivo.x;
    rectangulo.y=info->objetivo.y;

    UnloadImage(mapImage);//Se quitan de memoria las imagenes ya cargadas a la estrcutura Game
	UnloadImage(monedaImage);
    SetTargetFPS(60);

}


/**
 * Se pinta el mapa y los componentes
*/
void DrawGame(Game * game, InfoGame *info, InfoGame *oponente) {
    BeginDrawing();
        ClearBackground(RAYWHITE);//Se limpia la pantalla
        DrawTexture(game->map.texture, 0, 0, WHITE);//Se pinta el imagen de fondo
        DrawTexture(game->monedaTexture, info->objetivo.x, info->objetivo.y, WHITE);//Se pinta el imagen de fondo
        DrawCircleV(info->pos, 10, DARKBLUE);//Pintamos el cirulo con el radio indicado
        DrawCircleV(oponente->pos, 10, RED);//Pintamos el oponente
        DrawStatusText(game,mensaje, 30, YELLOW);
        
    EndDrawing();
}


void FinalizeGame() {
    pthread_mutex_destroy(&mutex);
	CloseWindow();        // Close window and OpenGL context
}
/**
 * Se espera hasta que el oponente este en linea
*/
void EsperarOponente(Config *config,Game *game,InfoGame *info,InfoGame *oponente,char * port){
    pthread_t hilo1;
    int s;
    struct sockaddr_in addr;
    int valopc;
    socklen_t len;
    if ( (s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        exit(EXIT_FAILURE);
    }
    valopc=1;
    len = sizeof(valopc);
    setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (void *)&valopc,len);
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(port)); //TODO argumento del main!
    addr.sin_addr.s_addr = INADDR_ANY;
    if (bind (s, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) != 0) {
        perror("bind");
        exit(EXIT_FAILURE);
    }
    if (listen(s, 10) != 0) {
        perror("bind");
        exit(EXIT_FAILURE);
    }
    //Recibir la conexion del cliente
    c = accept(s, NULL, 0);

    read(c,&oponente->pos,sizeof(oponente->pos));//Esperamos por ubicacion del oponenete
    write(c,&info->pos, sizeof(info->pos));//Le enviamos nuetra ubicacion al oponenete
    generarObjetivo(game,info);
    write(c,&info->objetivo,sizeof(info->pos));//Le enviamos al oponente el objetivo
    /*
    *Se crea el hilo que cambia la poscion de la moneda
    */
    config->game=game;
    config->info=info; 
    pthread_create(&hilo1,NULL,jumpers,config);
                                                

}
/**
 * Se procesan los eventos provenintes del teclado
 * que permiten mover la espera por el mapa
*/
void ProcessEvents(Game *game,InfoGame *info, InfoGame *oponente) {
    if(!info->flag && !oponente->flag){
        if (IsKeyDown(KEY_RIGHT)) {
            info->pos.x+= 2.0f;
            verificarObjetivo(info);//SE verifica si se ha alcanzado el objetivo
            write(c,info, sizeof(InfoGame));//Se le actualiza al oponente nuestra ubicacion
            
        }else
        if (IsKeyDown(KEY_LEFT)) {
            info->pos.x -= 2.0f;
            verificarObjetivo(info);
            write(c,info, sizeof(InfoGame));
            
        }else
        if (IsKeyDown(KEY_UP)) {
            info->pos.y -= 2.0f;
            verificarObjetivo(info);
            write(c,info, sizeof(InfoGame));
            
        }else
        if (IsKeyDown(KEY_DOWN)) {
            info->pos.y += 2.0f;
            verificarObjetivo(info);
            write(c,info, sizeof(InfoGame));

        }
        
    }else{
        //El oponente llego al objetivo primero o nosotros lo hicimos
        generarObjetivo(game,info);//pendiente usar mutex para el hilo
        //lock(&mutex);
    }
    FD_ZERO(&read_fds); //Inicializar el conjunto de descriptores
    FD_SET(c,&read_fds);
        
    if(select(c+1,&read_fds,0,0,&t)>=0){//Es verdadero cuando se detecta se ha realizado una escritura sobre
        if(FD_ISSET(c,&read_fds)){//Selector del oponente
            read(c,oponente,sizeof(InfoGame));//Se lee el cambio enviado
            printf("Op: %d\n",oponente->flag);
            //Validamos si el cliente(Oponente) llego primero a la moneda
            
        }
    }
}
/**
 * Se crea al objetivo, sera compartido entre los dos jugadores
*/
void generarObjetivo(Game *game,InfoGame *info){

    srand(time(NULL));
    int point_x=rand()%game->screen.width;
    int point_y=rand()%game->screen.height;
    info->objetivo.x=point_x;//Se guarda la poscion del obejtivo
    info->objetivo.y=point_y;
    //Le informamos al cliente de la nueva posicion del obejtivo
    write(c,info, sizeof(InfoGame));
    rectangulo.x=info->objetivo.x;
    rectangulo.y=info->objetivo.y;
    info->flag=0;//Estamos ahora listos para jugar

}
/**
 * Se valida si esta en colision el circulo con la moneda
 * Lo que indica que se ha alcanzado el objetivo
*/
void verificarObjetivo(InfoGame *info){
    //Cambiar para que solo busque cuando se mueva el objetivo
    if(CheckCollisionCircleRec(info->pos, 7,rectangulo)){
        info->flag=1;
        score+=1;
        char punt[512];
        sprintf(punt,"%d",score);
        strcpy(mensaje,"Score: ");
        strcat(mensaje,punt);
    }   
}

/*
 * Funcion utilizada por un hilo que permite generar de forma aleatoria 
 * la posicion de la moneda por un delta de t, una vez se genera la nueva posicion
 * Se duerme mientras transcurre el tiempo tambien aleatorio
 * Esta nueva ubicacion de la moneda sera visible en el oponente tambie
 */ 
void * jumpers(void * argv){
    Config *config=(Config*)argv;
    InfoGame *info=config->info;
    Game *game=config->game;
    int seg=0;
    //Sembramos la semilla
    srand(time(NULL));
    while(1){
        if(!info->flag){
            generarObjetivo(game,info);
            seg=1+rand()%(6-1);
            sleep(seg);
            
        }
    }

}
/**
* @brief Dibuja un exto en la esquina superior de la pantalla
* @param game Referencia a la estructura de información del juego.
* @param text Texto a dibujar
* @param size Tamaño de la fuente
* @param color Color de la fuente
*/
void DrawStatusText(Game * game, const char * text, int size, Color color) {
	DrawTextEx(game->font, text, (Vector2){0.0f, 0.0f }, size, 0, color);
}


