/****************************************************************************
* Copyright (C) 2019 by Erwin Meza Vega                                    *
*                                                                          *
* This file is part of Drones.                                             *
*                                                                          *
*   Drones is free software: you can redistribute it and/or modify it      *
*   under the terms of the GNU Lesser General Public License as published  *
*   by the Free Software Foundation, either version 3 of the License, or   *
*   (at your option) any later version.                                    *
*                                                                          *
*   Drones is distributed in the hope that it will be useful,              *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*   GNU Lesser General Public License for more details.                    *
*                                                                          *
*   You should have received a copy of the GNU Lesser General Public       *
*   License along with Drones.  If not, see <http://www.gnu.org/licenses/>.*
****************************************************************************/

#include "game.h"
#include "raylib.h"
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	
	if(argc!=3){
		perror("Arguments use: <host> <port>");
        exit(EXIT_FAILURE);
	}else{
		Play(argv[1],argv[2]);
	}
	
	return 0;
}


