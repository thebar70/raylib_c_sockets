#ifndef GAME_H
#define GAME_H 

#include "raylib.h"

typedef struct {
	int width; /**< Ancho de la pantalla en pixels. */
	int height; /**< Alto de la pantalla en pixels. */
}GameScreen;

/** 
* @brief Mapa del juego
*/
typedef struct {
	Texture2D texture;	/**< Imagen de fondo. */
	int width; /**< Ancho del mapa. */
	int height; /**< Alto del mapa. */
	Rectangle base;	/**< Rectángulo base del mapa. */
}GameMap;


/** 
* @brief Estructura de información para el juego.
*/
typedef struct {
	
	GameMap map; /**< Mapa del juego. */
	GameScreen screen; /**< Pantalla del juego. */
	Texture2D monedaTexture; /*Imagen del objetivo*/
	double minZoom; /**< Nivel minimo de acercamiento. */
	double maxZoom; /**< Nivel maximo de acercamiento. */
	Font font; /**< Fuente para dibujar los textos. */
	Rectangle base_moneda;
	
}Game;

typedef struct 
{
	Vector2 pos;
	Vector2 objetivo;
	int flag;
}InfoGame;





void Play(char * host, char * port);

void configuracionGeneral(Game *game,InfoGame *info);

void generarObjetivo(Game *game,InfoGame *info);

void  verificarObjetivo(InfoGame *info);

void InitGame(Game * game,InfoGame *info);

void ConectarOponente(InfoGame *info, InfoGame *oponente,char * host,char * port);

void ProcessEvents(Game *game,InfoGame *info, InfoGame *oponente);

void DrawGame(Game * game, InfoGame *info, InfoGame *oponente);

void DrawStatusText(Game * game, const char * text, int size, Color color);


void FinalizeGame();


#endif
