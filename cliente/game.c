#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* Según POSIX 1003.1-2001 */
#include <sys/select.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <math.h>

#include <sys/socket.h>
#include <pthread.h>
#include <semaphore.h>

/* Según estándares anteriores */
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include "game.h"

#include "raylib.h"


fd_set read_fds;
struct timeval t;
Rectangle rectangulo;
int score;
char * mensaje;
int s;//Descriptor del servidor
void Play(char * host, char * port){
    //Configuracion general
    Game *game;
    InfoGame *info;
    InfoGame *oponente;
    info=(InfoGame*)malloc(sizeof( InfoGame));
    oponente=(InfoGame*)malloc(sizeof( InfoGame));
    game=(Game *)malloc(sizeof( Game));
    t.tv_sec=0;
    t.tv_usec=10;

    //Iniciando conexion con el oponente
    configuracionGeneral(game,info); 
    ConectarOponente(info,oponente,host,port);
	InitGame(game,info);
	// Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
	{
		// Update
		//----------------------------------------------------------------------------------
		ProcessEvents(game,info,oponente); 
        DrawGame(game,info,oponente);
    }
	FinalizeGame(game);
}
void configuracionGeneral(Game * game, InfoGame *info){
   
    //Definicion del tamaño de la ventana
    game->screen.width=800;
    game->screen.height=450;
    t.tv_sec=0;
    t.tv_usec=10;
    score=0;
    mensaje=(char*)malloc (sizeof(char));
    strcpy(mensaje,"Score: 0");

    //Posicion de la esfera
    info->pos=(Vector2) { (float)game->screen.width/2, (float)game->screen.height/2 };
    info->flag=0;
    //*************************************///
}
void InitGame(Game * game,InfoGame *info) {
    //Cargando la imagen principal en la ventana
    Image mapImage = LoadImage("resources/mapa3.jpg");
    Image monedaImage = LoadImage("resources/moneda.png"); 
    game->map.width = mapImage.width;
	game->map.height = mapImage.height;
    InitWindow(game->screen.width, game->screen.height, "Cliente "); 
    game->map.base = (Rectangle){0, 0,  mapImage.width, mapImage.height};
    game->map.texture = LoadTextureFromImage(mapImage);
    game->monedaTexture=LoadTextureFromImage(monedaImage);
    game->base_moneda = (Rectangle){0, 0,  monedaImage.width, monedaImage.height};
    game->font = LoadFont("resources/Hemondalisa.ttf");
    SetTextureFilter(game->font.texture, FILTER_BILINEAR);//Establecer el filtro para el trazado de la fuente
    rectangulo=game->base_moneda;//Creamos el regtangulo que contiene la base de la moneda
    rectangulo.x=info->objetivo.x;
    rectangulo.y=info->objetivo.y;

    UnloadImage(mapImage);//Se quitan de memoria las imagenes ya cargadas a la estrcutura Game
	UnloadImage(monedaImage);
    SetTargetFPS(60);

}


/**
 * Se pinta el mapa y los componentes
*/
void DrawGame(Game * game, InfoGame *info, InfoGame *oponente) {
    BeginDrawing();
        ClearBackground(RAYWHITE);//Se limpia la pantalla
        DrawTexture(game->map.texture, 0, 0, WHITE);//Se pinta el imagen de fondo
        //Se indica que es el oponente quien maneja la posicion de la moneda
        DrawTexture(game->monedaTexture, oponente->objetivo.x, oponente->objetivo.y, WHITE);//Se pinta el imagen de fondo
        DrawCircleV(info->pos, 10, DARKBLUE);//Pintamos el cirulo con el radio indicado
        DrawCircleV(oponente->pos, 10, RED);//Pintamos el oponente
        DrawStatusText(game,mensaje, 30, YELLOW);
        
    EndDrawing();
}


void FinalizeGame() {
	CloseWindow();        // Close window and OpenGL context
}

void ConectarOponente(InfoGame *info, InfoGame *oponente, char *host, char * port){
    struct sockaddr_in addr;
    if ( (s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        exit(EXIT_FAILURE);
    }
    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(port)); //TODO argumento del main!
    //TODO tomar la IP de los argumentos del main!!
    if (inet_aton(host,&addr.sin_addr) == 0) {
        fprintf(stderr, "Invalid address!\n");
        exit(EXIT_FAILURE);
    }
    if (connect (s, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) != 0) {
        perror("connect");
        exit(EXIT_FAILURE);
    }
    
    write(s,&info->pos,sizeof(info->pos));//Le enviamos nuetra posicion inicial al oponete
    read(s,&oponente->pos,sizeof(oponente->pos));//Esperamos ubicacion inicial del oponente
    read(s,&info->objetivo,sizeof(info->pos));//Esperamos ubicacion inicial del oponente
   
}

void ProcessEvents(Game *game,InfoGame *info, InfoGame *oponente) {
    if(!info->flag){
        if (IsKeyDown(KEY_RIGHT)) {
            info->pos.x+= 2.0f;
            verificarObjetivo(info);
            write(s,info, sizeof(InfoGame));
            
        }else
        if (IsKeyDown(KEY_LEFT)) {
            info->pos.x -= 2.0f;
            verificarObjetivo(info);
            write(s,info, sizeof(InfoGame));
            
        }else
        if (IsKeyDown(KEY_UP)) {
            info->pos.y -= 2.0f;
            verificarObjetivo(info);
            write(s,info, sizeof(InfoGame));
            
        }else
        if (IsKeyDown(KEY_DOWN)) {
            info->pos.y += 2.0f;
            verificarObjetivo(info);
            write(s,info, sizeof(InfoGame));

        }
        
    }
    FD_ZERO(&read_fds); //Inicializar el conjunto de descriptores
    FD_SET(s,&read_fds);        
    if(select(s+1,&read_fds,0,0,&t)>=0){
        if(FD_ISSET(s,&read_fds)){
            read(s,oponente,sizeof(InfoGame));
            printf("Leyendo--\n");
            //Se actualiza la posicion de la moneda de haberse presentado un cambio
            if(oponente->objetivo.x!=rectangulo.x && oponente->objetivo.y!=rectangulo.y){
                rectangulo.x=oponente->objetivo.x;
                rectangulo.y=oponente->objetivo.y;//Se utiliza para cuando se valide las colisiones
                info->flag=0; //Si nosotros fuimos los que generamos el cambio, nos habilitamos
                //Para seguir jugando
                write(s,info, sizeof(InfoGame));//Le informamos el oponente que estamos nuevamente listos
            }
        }
    }
}
/**
 * Se valida si esta en colision el circulo con la moneda
 * Lo que indica que se ha alcanzado el objetivo
*/
void verificarObjetivo(InfoGame *info){
    //Cambiar para que solo busque cuando se mueva el objetivo
    if(CheckCollisionCircleRec(info->pos, 7,rectangulo) && !info->flag){
        info->flag=1;
        score+=1;
        char punt[512];
        sprintf(punt,"%d",score);
        strcpy(mensaje,"Score: ");
        strcat(mensaje,punt);
    }
}

/**
* @brief Dibuja un exto en la esquina superior de la pantalla
* @param game Referencia a la estructura de información del juego.
* @param text Texto a dibujar
* @param size Tamaño de la fuente
* @param color Color de la fuente
*/
void DrawStatusText(Game * game, const char * text, int size, Color color) {
	DrawTextEx(game->font, text, (Vector2){0.0f, 0.0f }, size, 0, color);
}



